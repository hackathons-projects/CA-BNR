import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:convert/convert.dart';
import 'package:encrypt/encrypt.dart';
import 'package:pointycastle/api.dart';
import 'package:pointycastle/asymmetric/api.dart';
import 'package:pointycastle/asymmetric/rsa.dart';
import 'package:simple_rsa/simple_rsa.dart';

class CryptoService {


  final parser = RSAKeyParser();

  final pubRsaBase64 = 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAupR7+UF/LwN' +
      'rdKE+mCom2eFZJgatk5Vbj/4alBttVjtKymzIlDRnVavWoD/nu6Ynl5to2esJocqEQ2g2vA/' +
      'HUCJdhRbILqW54L/Cc7C6qSWOnVgQ2t4b/7AD6yMF3TT3m8CZAMGkBLshWfNsmdZuH1WYrYy' +
      'fnvFdmWpITaP59izxAxOEI96bJlSwL9Z04r18BUY9jr2WYhgQ4gQNUzKTvgJtt3kzRn1PZW4' +
      'Sq1sYIqURDh7vrdqakWBAFlHtgTom0N7U9+mNq57NWR2qLHunUcGQ7ACQ5v4cNU3rP0FFmfE' +
      'YAPJfbWxEtr2Hk3jtFqB5LyzZhNhTYdyyVUa589RezQIDAQAB';

  final rsaKeyBase64 = 'MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC6lHv' +
      '5QX8vA2t0oT6YKibZ4VkmBq2TlVuP/hqUG21WO0rKbMiUNGdVq9agP+e7pieXm2jZ6wmhyoR' +
      'DaDa8D8dQIl2FFsgupbngv8JzsLqpJY6dWBDa3hv/sAPrIwXdNPebwJkAwaQEuyFZ82yZ1m4' +
      'fVZitjJ+e8V2ZakhNo/n2LPEDE4Qj3psmVLAv1nTivXwFRj2OvZZiGBDiBA1TMpO+Am23eTN' +
      'GfU9lbhKrWxgipREOHu+t2pqRYEAWUe2BOibQ3tT36Y2rns1ZHaose6dRwZDsAJDm/hw1Tes' +
      '/QUWZ8RgA8l9tbES2vYeTeO0WoHkvLNmE2FNh3LJVRrnz1F7NAgMBAAECggEBAJAld0Iy39e' +
      'qhLIugPV+W1WpS/6c2i1TDtJINrCGIAqm9Dk/ohceBVei0sYrmRTYYW43muIBPAfLNjP9p4v' +
      'ThODcK/ROjYm8b8a7X7eRqiRT58KX7y2ou2jmy6A2BqnH6iRiv9JdyCH/kNy1vAl+KMX8k2B' +
      'BbHCXeQC1o/aX4N3WuCFS0i5xjAFcAvouicPii6JPTK7glY766MgjA8zgFxouRLY1O0ZcDtA' +
      'jfUnDcTkYvzwBl5J8J0FOl/ks9kvfWWpIC/WGnj9m1p7rRHbkNW+mSlMzmkPg196jSrSil1a' +
      'Ki4OakSHsCVz2yLm3yR2fGzL4nzYwvtEzKoqcC0uK3+ECgYEA6zMeeXovJkElvVEas7obnwI' +
      'fsiusptBlpjk8HAOfso3qLZoaQI2SFeaRTIs6T1UWcIBHExISjjB3USlDpV7EidXZM4Oc/pB' +
      'isnoeF68mTru5QUD2G1wzJD5Cm0Bq68xp0Bno7wiYqVMY/pdNKnuUReh3bk2xARa3C2haVDZ' +
      'IFwMCgYEAyxSfzGflmucA5WjmCz+gNRXIWDAnr580VtLzKZNYnQUxIcvGprN2LiYQ2C31NLz' +
      'Xg5v0pKW2hnc1QOqvT5KtFQYtrw6tx1cSvGTqHgbbDDMJHDW1pqZIQoO/1A+YxIqThsToaK+' +
      'XFD62WB7VOdySzzxqVb5AqFFycf1FmPNaoe8CgYEAkP+j0vo7pRbWwF8G2jRrIN07UylPuEu' +
      'TzyVL5G1rf7Wz4Ec31gIxRKUvgIP4/72SJus+ZghnPhetB3EsDgBdpCgdTn/eQqYa4rMcaDe' +
      'Ho1RiAlOdJgLyWG61u2a8RAByX2QJ0Yt4KjNbnmECpqUnJO4K6wqki+pIEJVV0sLgMKcCgYB' +
      'BPlQEeJCkjniUO3pEvOE8jM37vyTQ4GYRSt6CSetbLvLU98HPhrJ+Kw6YVVOE6PKYPr9/Mho' +
      'M4zmBrKQ7/VSL/5sntaCQ/WFwkz8//FXdenv/yyWb76ohbHBxsb0Tz62Ly7EbCOaRw4ATmXx' +
      'JhZrI55EGEQR2zwvkoUj91ZkVkwKBgDfWcPdbVvjQRBUDUffNC1PRzLgvj0pNvK31PJM8owG' +
      '2z7E0N7dc5JE60HikWzANjTxYKOHKAg1Iu6CKYLDeZ9IYUA0ej/TcqABPHWZKVH2Tw6jSo5B' +
      'N41JFe1WfHyv/csWTjOLlzPbN2Pj3rWocwwip23wV7TDHuwDiI3MUSNNP';

  final encrypter = Encrypter(AES(
      Key.fromBase64('vlM8BvMLT6GFV2NJK0lei4rLoXXQsodoNKzP1SjK2t8='),
      mode: AESMode.ecb));

  Future<String> encode(String input) async {
    var qrTicket = encrypter.decrypt(Key.fromBase64(input));
    Map qrMap = json.decode(qrTicket);
    var signature = qrMap['signature'];

    // TODO flutter rsa decrypt
    // final decryptedText = await decryptString(
    //     signature, utf8.decode(base64.decode(rsaKeyBase64)));
    //
    // qrMap['signature'] =
    //     qrMap['startTripDate'] + '/' + 'endTripDate' == decryptedText
    //         ? 'OK'
    //         : 'FAILURE';
    qrMap['signature'] = 'OK';

    return json.encode(qrMap);
  }


}
