package ru.catanalysis.cat_analysis_ticket.model;

public enum AuthType {
    QR, BLE, NFC, FACE, VOICE
}
