package ru.catanalysis.cat_analysis_ticket.model;

public enum TicketStatus {
    REQUEST, APPROVED, REVOKED, EXPIRED
}
