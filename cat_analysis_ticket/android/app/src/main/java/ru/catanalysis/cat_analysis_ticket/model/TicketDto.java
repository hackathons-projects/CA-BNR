package ru.catanalysis.cat_analysis_ticket.model;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashSet;
import java.util.Set;

@JsonPropertyOrder(alphabetic=true)
public class TicketDto {

    public Long id;

    public Set<Long> checkPointIds = new HashSet<>();

    public String email;

    public String name;

    public String phone;

    public String activationDate;

    public String expirationDate;

    public TicketStatus status;

    public TicketType type;

    public Set<AuthType> authTypes;

}
