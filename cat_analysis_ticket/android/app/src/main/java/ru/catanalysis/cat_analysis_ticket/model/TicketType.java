package ru.catanalysis.cat_analysis_ticket.model;

public enum TicketType {
    TEMPORARY, REGULAR, DISPOSABLE
}
