package ru.catanalysis.bnr.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.catanalysis.bnr.model.TicketDto;
import ru.catanalysis.bnr.model.request.BuyTicketRequest;
import ru.catanalysis.bnr.service.CryptoService;
import ru.catanalysis.bnr.service.TicketService;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.validation.Valid;
import java.util.UUID;

@PreAuthorize("isAuthenticated()")
@RequiredArgsConstructor
@RestController
@RequestMapping("/tickets")
public class TicketController {

    private final TicketService ticketService;
    private final CryptoService cryptoService;

    @PostMapping
    public ResponseEntity<TicketDto> buyTicket(@Valid @RequestBody BuyTicketRequest request) {
        return ResponseEntity.ok(ticketService.buyTicket(request));
    }

    @GetMapping("/{ticketId}/qr")
    public ResponseEntity<String> getTicketQRBase64(@PathVariable Long ticketId) {
        return ResponseEntity.ok(ticketService.generateQRBase64ByTicketId(ticketId));
    }

    @GetMapping("/{ticketId}/encrypted-qr")
    public ResponseEntity<String> getEncryptedTicketQRBase64(@PathVariable Long ticketId) throws BadPaddingException, IllegalBlockSizeException {
        return ResponseEntity.ok(ticketService.generateEncryptedQRBase64ByTicketId(ticketId));
    }

    @GetMapping("/{ticketId}/qr-file")
    public ResponseEntity<Resource> getTicketQR(@PathVariable Long ticketId) {
        InputStreamResource resource = new InputStreamResource(ticketService.generateQRBase64ByTicketIdAsStream(ticketId));
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, UUID.randomUUID().toString() + TicketService.QR_EXTENSION)
                .contentType(MediaType.parseMediaType(TicketService.QR_MEDIA_TYPE))
                .body(resource);
    }

    @GetMapping("/{ticketId}/encrypted-qr-file")
    public ResponseEntity<Resource> getEncryptedTicketQR(@PathVariable Long ticketId) {
        InputStreamResource resource = new InputStreamResource(ticketService.generateEncryptedQRBase64ByTicketIdAsStream(ticketId));
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, UUID.randomUUID().toString() + TicketService.QR_EXTENSION)
                .contentType(MediaType.parseMediaType(TicketService.QR_MEDIA_TYPE))
                .body(resource);
    }
}
