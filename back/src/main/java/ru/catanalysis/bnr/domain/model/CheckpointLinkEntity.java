package ru.catanalysis.bnr.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "checkpoint_links")
public class CheckpointLinkEntity extends BaseEntity<Long> {

    @ManyToOne
    @JoinColumn(name = "destination_id", nullable = false)
    private CheckPointEntity destination;

    @Column
    private Double weight;
}
