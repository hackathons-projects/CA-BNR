package ru.catanalysis.bnr.domain.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import ru.catanalysis.bnr.domain.model.CheckPointEntity;

import java.util.Collection;
import java.util.Set;

public interface CheckPointRepository extends PagingAndSortingRepository<CheckPointEntity, Long> {

    Set<CheckPointEntity> findAllByIdIn(Collection<Long> id);
}
