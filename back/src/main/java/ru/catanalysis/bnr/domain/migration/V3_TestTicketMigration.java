package ru.catanalysis.bnr.domain.migration;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.catanalysis.bnr.domain.model.TicketEntity;
import ru.catanalysis.bnr.domain.model.TrackEntity;
import ru.catanalysis.bnr.domain.repository.TicketRepository;
import ru.catanalysis.bnr.domain.repository.TrackRepository;

import java.time.LocalDate;
import java.util.HashSet;

@RequiredArgsConstructor
@Transactional
@Component
public class V3_TestTicketMigration implements Migration {

    private final TicketRepository ticketRepository;
    private final TrackRepository trackRepository;

    @Override
    public String getId() {
        return V3_TestTicketMigration.class.getName();
    }

    @Override
    public void migrate() {
        TrackEntity track = trackRepository.findAll().stream().findFirst().orElseThrow();
        TicketEntity ticketEntity = new TicketEntity(track, track.getNaturalReserve(), new HashSet<>(track.getCheckPoints()),
                "admin@ca.by", "Admin", 10, "+...",
                LocalDate.now().plusDays(1), LocalDate.now().plusDays(2));
        ticketRepository.save(ticketEntity);
    }
}
