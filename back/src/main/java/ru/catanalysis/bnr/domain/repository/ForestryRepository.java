package ru.catanalysis.bnr.domain.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import ru.catanalysis.bnr.domain.model.ForestryEntity;

public interface ForestryRepository extends PagingAndSortingRepository<ForestryEntity, Long> {
}
