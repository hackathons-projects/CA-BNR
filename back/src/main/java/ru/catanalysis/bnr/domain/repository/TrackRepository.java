package ru.catanalysis.bnr.domain.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import ru.catanalysis.bnr.domain.model.TrackEntity;

import java.util.List;

public interface TrackRepository extends PagingAndSortingRepository<TrackEntity, Long> {

    List<TrackEntity> findAll();
}
