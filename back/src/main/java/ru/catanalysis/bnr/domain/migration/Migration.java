package ru.catanalysis.bnr.domain.migration;

/**
 * Conventional naming by order
 */
public interface Migration {
    String getId();

    void migrate();
}
