package ru.catanalysis.bnr.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.catanalysis.bnr.domain.model.MigrationEntity;

public interface MigrationRepository extends JpaRepository<MigrationEntity, Long> {

    boolean existsByClassName(String className);
}
