package ru.catanalysis.bnr.domain.model;

public enum Gender {
    MALE, FEMALE, NONE
}
