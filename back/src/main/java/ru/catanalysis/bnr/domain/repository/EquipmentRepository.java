package ru.catanalysis.bnr.domain.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import ru.catanalysis.bnr.domain.model.EquipmentEntity;

public interface EquipmentRepository extends PagingAndSortingRepository<EquipmentEntity, Long> {
}
