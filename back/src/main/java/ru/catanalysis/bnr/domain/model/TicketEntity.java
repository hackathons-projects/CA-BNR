package ru.catanalysis.bnr.domain.model;

import lombok.*;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Audited
@EqualsAndHashCode(callSuper = true)
@ToString
@Entity
@Table(name = "tickets")
public class TicketEntity extends BaseEntity<Long> {

    @NotAudited
    @ManyToOne
    @JoinColumn(name = "track_id")
    private TrackEntity track;

    @NotAudited
    @ManyToOne
    @JoinColumn(name = "natural_reserve_id", nullable = false)
    private NaturalReserveEntity naturalReserve;

    @NotAudited
    @ManyToMany
    private Set<CheckPointEntity> checkPoints = new HashSet<>();

    @Column
    private String email;

    @Column
    private String name;

    @Column
    private Integer quantity;

    @Column
    private String phone;

    @Column
    private LocalDate startTripDate;

    @Column
    private LocalDate endTripDate;

}
