package ru.catanalysis.bnr.domain.migration;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.catanalysis.bnr.domain.model.NaturalReserveEntity;
import ru.catanalysis.bnr.domain.model.TrackEntity;
import ru.catanalysis.bnr.domain.repository.NaturalReserveRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

@RequiredArgsConstructor
@Transactional
@Component
public class V2_BerezinskyMigration implements Migration {

    private final NaturalReserveRepository naturalReserveRepository;

    @Override
    public String getId() {
        return V2_BerezinskyMigration.class.getName();
    }

    @Override
    public void migrate() {
        NaturalReserveEntity naturalReserve = naturalReserveRepository.save(new NaturalReserveEntity(
                "Государственное природоохранное учреждение «Березинский биосферный заповедник»",
                new HashSet<>(), new HashSet<>(), new ArrayList<>()));

        naturalReserve.getTracks().add(new TrackEntity(
                naturalReserve,
                naturalReserve.getCheckPoints(),
                "Экстремальный маршрут «Люди на болоте»",
                "Протяженность маршрута составляет 6 км, продолжительность - 4часа. Туристический маршрут включает 4 остановочного пункта.\n" +
                        "\n" +
                        "\n" +
                        "Маршрут проходит по Рожнянскому болоту. Он подходит для любителей активного туризма, готовых бросить вызов дикому верховому болоту и даже самому себе. Помимо экстремального похода гостей ожидает девственная красота заповедной природы и увлекательный рассказ, раскрывающий страничка за страничкой тайны этого удивительного места.\n" +
                        "\n" +
                        "Стоимость экскурсии:\n" +
                        "\n" +
                        "для 1 чел. при группе до 10 чел. в будние дни - 40 руб.\n" +
                        "для 1 чел. при группе от 10 до 20 чел. в будние дни - 20 руб.\n" +
                        "для 1 чел. при группе до 10 чел. в выходные дни - 50 руб.\n" +
                        "для 1 чел. при группе от 10 до 20 чел. в выходные дни - 30 руб.",
                6.0, null, null, null, null, null, null,
                Arrays.asList("fd823702ba0f96f75f4415f4d7bec044.jpg", "0b3407113a5762f7e4b1a3baadb5673d.png")
        ));

        naturalReserve.getTracks().add(new TrackEntity(
                naturalReserve, naturalReserve.getCheckPoints(),
                "Каменная плита",
                "Протяженность маршрута составляет 7,5 км, продолжительность - 3,5 часа. Туристический маршрут включает 10 остановочных пунктов.\n" +
                        "\n" +
                        "Основной объект маршрута - памятник природы «Валун». Он представляет собой каменную, расколотую на две половины, плиту, которая выступает над поверхностью земли на 15 – 20 см и практически не заметна в лесу. Протяженность камня по длинной оси – 5 х 2,5 м., по короткой – 3 х 1,5 м. Чуть дальше, примерно на расстоянии в 2,5 метра к северу от плиты, находится еще один камень. Он имеет правильную прямоугольную форму и расположен по длине строго с запада на восток. Этот комплекс камней также образовался в результате отступления Валдайского ледника.\n" +
                        "\n" +
                        "Экскурсионное обслуживание по маршруту «Каменная плита» (5 академических часов): \n" +
                        "\n" +
                        "до 20 человек – 50 BYN\n",
                7.5, null, null, null, null, null, null,
                Arrays.asList("67a79659191926824fcd5c5df170debb.jpg", "665d29af1fc64e887b5c6a0a863a95b9.jpg")
        ));

        naturalReserve.getTracks().add(new TrackEntity(
                naturalReserve, naturalReserve.getCheckPoints(),
                "По лесной заповедной тропе (экологическая тропа)",
                "В любую пору года здесь найдется много интересного для любознательного человека.\n" +
                        "\n" +
                        "Всего за пару часов перед вами сменяя друг друга раскроют свои тайны сумрачный еловый лес и сыроватый осинник,  обилием света порадуют сосновый лес на краю болота и светлый березняк с зарослями орешника.\n" +
                        "\n" +
                        "С 15-ти метровой смотровой вышки откроется величественная панорама окутанного туманом верхового болота, где ковер из сфагнового мха, плодоносящая пушица, насекомоядная росянка и клюква оживляют болото в разное время года. \n" +
                        "\n" +
                        "Маршрут оборудован интересными информационными стендами в рамках реализации в Березинском биосферном заповеднике совместного проекта Европейского Союза и Программы развития ООН «Повышение экологической информированности молодежи через учреждение и развитие Зеленых школ в Беларуси».\n" +
                        "\n" +
                        "Стоимость экскурсии «По лесной заповедной тропе»:\n" +
                        "\n" +
                        "- Сборная группа 1-4 человека – 20 BYN, за каждого последующего – 5 BYN. Максимальный размер группы – 20 человек.\n" +
                        "\n" +
                        "\n" +
                        "- Индивидуальное экскурсионное обслуживание на группу 1-5 человек – 40 BYN.",
                null, null, null, null, null, null, null,
                Arrays.asList("bc5b7b4d25bc40ff56cd32a265f203f7.jpeg", "b813c302dcbfb51099fd737ce1d549c4.jpg", "83e705a53cb2f68cde3d222e47819a69.jpeg",
                        "8d5495c91bdc74eb94eb12528090e7dd.jpeg", "51e6d25c64602db89611b8c0b7d2f334.jpeg", "b1f876463e3771b3fba11c8d0b41d214.jpeg")
        ));

        naturalReserve.getTracks().add(new TrackEntity(
                naturalReserve, naturalReserve.getCheckPoints(),
                "Тропа открытий",
                "Протяженность – 1 км. Продолжительность – 3 часа (академических).\n" +
                        "\n" +
                        "Интерактивная экскурсия, которая проходит по территории дендропарка. Информативность экологической тропы направлена на школьников начальных и средних классов.\n" +
                        "\n" +
                        "Следуя по тропе, вы познакомитесь с насекомыми, обитающими на лугу. Узнаете, из каких деревьев и кустарников состоит лес и кто в нем живет, а также познакомитесь с беспозвоночными – обитателями водоема.\n" +
                        "\n" +
                        "Стоимость интерактивной экскурсии \"Тропа открытий\" (40 мин.):\n" +
                        "до 25 чел. - 20 руб\n" +
                        "до 45 чел. - 40 руб.",
                1., null, null, null, null, null, null,
                Arrays.asList("f38d670e4908e5372480a557b8cc75a8.jpeg", "4e9fa1e4600a74ce4ce25fe6c771a282.jpg", "13e37c050ccdff188b210990b8db76c6.jpg",
                        "e8a1befe671bf66b424eba13f11d4c02.jpeg")
        ));

        naturalReserveRepository.save(naturalReserve);
    }
}
