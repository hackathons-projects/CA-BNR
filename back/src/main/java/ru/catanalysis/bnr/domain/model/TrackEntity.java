package ru.catanalysis.bnr.domain.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true, exclude = {"naturalReserve"})
@ToString(exclude = {"naturalReserve"})
@Entity
@Table(name = "tracks")
public class TrackEntity extends BaseEntity<Long> {

    @ManyToOne
    @JoinColumn(name = "natural_reserve_id", nullable = false)
    private NaturalReserveEntity naturalReserve;

    @OneToMany(mappedBy = "track", cascade = CascadeType.ALL)
    private Set<CheckPointEntity> checkPoints = new HashSet<>();

    @Column
    private String name;

    @Lob
    @Column
    private String description;

    @Column
    private Double distance;

    @Column
    private String seasonRecommendations;

    @Column
    private String recommendedTime;

    @Column
    private String region;

    @Column
    private String district;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "forestry_id")
    private ForestryEntity forestry;

    @Column
    private BigDecimal amount;

    @ElementCollection
    private List<String> photos = new ArrayList<>();
}
