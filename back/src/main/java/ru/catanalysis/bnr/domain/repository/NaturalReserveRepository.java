package ru.catanalysis.bnr.domain.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import ru.catanalysis.bnr.domain.model.NaturalReserveEntity;

public interface NaturalReserveRepository extends PagingAndSortingRepository<NaturalReserveEntity, Long> {
}
