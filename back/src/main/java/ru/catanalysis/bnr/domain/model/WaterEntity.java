package ru.catanalysis.bnr.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "waters")
public class WaterEntity extends BaseEntity<Long> {

    @Column
    private String name;

    @Lob
    @Column
    private String description;

    @ElementCollection
    private List<String> photos = new ArrayList<>();
}
