package ru.catanalysis.bnr.domain.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import ru.catanalysis.bnr.domain.model.TicketEntity;

public interface TicketRepository extends PagingAndSortingRepository<TicketEntity, Long> {
}
