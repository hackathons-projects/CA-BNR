package ru.catanalysis.bnr.domain.model;

public enum Role {
    USER, ADMIN
}
