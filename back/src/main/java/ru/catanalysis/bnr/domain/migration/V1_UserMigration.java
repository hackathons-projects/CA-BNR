package ru.catanalysis.bnr.domain.migration;


import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.catanalysis.bnr.domain.model.Gender;
import ru.catanalysis.bnr.domain.model.Role;
import ru.catanalysis.bnr.domain.model.UserEntity;
import ru.catanalysis.bnr.domain.repository.UserRepository;

import java.time.LocalDateTime;

@RequiredArgsConstructor
@Transactional
@Component
public class V1_UserMigration implements Migration {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public String getId() {
        return V1_UserMigration.class.getName();
    }

    @Override
    public void migrate() {
        userRepository.save(new UserEntity(
                "admin@ca.by",
                passwordEncoder.encode("admin"),
                Role.ADMIN,
                "Admin",
                LocalDateTime.now(),
                Gender.NONE,
                "XXXXXXXXXX",
                "Minsk"
        ));
    }
}
