package ru.catanalysis.bnr.domain.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import ru.catanalysis.bnr.domain.model.WaterEntity;

public interface WaterRepository extends PagingAndSortingRepository<WaterEntity, Long> {
}
