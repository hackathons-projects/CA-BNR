package ru.catanalysis.bnr.domain.model;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
@Entity
@Table(name = "natural_reserves")
public class NaturalReserveEntity extends BaseEntity<Long> {

    @Column
    private String name;

    @OneToMany(mappedBy = "naturalReserve", cascade = CascadeType.ALL)
    private Set<TrackEntity> tracks = new HashSet<>();

    @OneToMany(mappedBy = "naturalReserve", cascade = CascadeType.ALL)
    private Set<CheckPointEntity> checkPoints = new HashSet<>();

    @ElementCollection
    private List<String> photos = new ArrayList<>();
}
