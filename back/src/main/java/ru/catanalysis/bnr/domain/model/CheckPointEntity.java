package ru.catanalysis.bnr.domain.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true, exclude = {"naturalReserve", "track"})
@ToString(exclude = {"naturalReserve", "track"})
@Entity
@Table(name = "checkpoints")
public class CheckPointEntity extends BaseEntity<Long> {

    @Column
    private String name;

    @Column
    private String description;

    @Column
    private Integer capacity;

    @Column
    private BigDecimal amount;

    @Column
    private BigDecimal weekendAmount;

    @ManyToMany
    private Set<WaterEntity> waters;

    @ManyToMany
    private Set<EquipmentEntity> equipments = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "natural_reserve_id", nullable = false)
    private NaturalReserveEntity naturalReserve;

    @ManyToOne
    @JoinColumn(name = "track_id")
    private TrackEntity track;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "forestry_id")
    private ForestryEntity forestry;

    @OneToMany(mappedBy = "destination", cascade = CascadeType.ALL)
    private Set<CheckpointLinkEntity> path = new HashSet<>();

    @ElementCollection
    private List<String> photos = new ArrayList<>();
}
