package ru.catanalysis.bnr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@ConfigurationPropertiesScan("ru.catanalysis.bnr")
@SpringBootApplication
public class BnrApplication {

    public static void main(String[] args) {
        SpringApplication.run(BnrApplication.class, args);
    }

}
