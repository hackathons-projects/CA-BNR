package ru.catanalysis.bnr.converter;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.catanalysis.bnr.domain.model.TicketEntity;
import ru.catanalysis.bnr.exception.CustomException;
import ru.catanalysis.bnr.model.QrTicketDto;
import ru.catanalysis.bnr.service.CryptoService;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.nio.charset.StandardCharsets;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Optional;
import java.util.stream.Collectors;

@Transactional
@Component
public class QrCodeConverter implements Converter<TicketEntity, QrTicketDto> {

    private final DateTimeFormatter FORMATTER;
    private final CryptoService cryptoService;

    public QrCodeConverter(@Value("${format.local-date}") String format, CryptoService cryptoService) {
        this.FORMATTER = DateTimeFormatter.ofPattern(format);
        this.cryptoService = cryptoService;
    }

    @Override
    public QrTicketDto convert(MappingContext<TicketEntity, QrTicketDto> mappingContext) {
        QrTicketDto result = Optional.ofNullable(mappingContext.getDestination()).orElse(new QrTicketDto());
        result.setId(mappingContext.getSource().getId());
        result.setTrackName(mappingContext.getSource().getTrack().getName());
        result.setNaturalReserveName(mappingContext.getSource().getNaturalReserve().getName());
        result.setCheckPointIds(mappingContext.getSource().getCheckPoints().stream().map(checkPointEntity -> checkPointEntity.getId()).collect(Collectors.toSet()));
        result.setEmail(mappingContext.getSource().getEmail());
        result.setName(mappingContext.getSource().getName());
        result.setQuantity(mappingContext.getSource().getQuantity());
        result.setPhone(mappingContext.getSource().getPhone());
        result.setStartTripDate(mappingContext.getSource().getStartTripDate().format(FORMATTER));
        result.setEndTripDate(mappingContext.getSource().getEndTripDate().format(FORMATTER));

        try {
            String stamp = result.getStartTripDate() + "/" + result.getEndTripDate();
            result.setSignature(Base64.getEncoder().encodeToString(cryptoService.getEncryptRsaCipher().doFinal(stamp.getBytes(StandardCharsets.UTF_8))));
        } catch (IllegalBlockSizeException | BadPaddingException e) {
            throw new CustomException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return result;
    }
}
