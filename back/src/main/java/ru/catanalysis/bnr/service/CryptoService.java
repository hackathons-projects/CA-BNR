package ru.catanalysis.bnr.service;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.catanalysis.bnr.configuration.CryptoConfig;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

@Slf4j
@Getter
@Transactional
@Service
public class CryptoService {

    private final CryptoConfig cryptoConfig;
    private final Cipher encryptRsaCipher;
    private final Cipher decryptRsaCipher;
    private final Cipher encryptAesCipher;
    private final Cipher decryptAesCipher;
    private final Cipher encryptHybridCipher;
    private final Cipher decryptHybridCipher;

    public CryptoService(CryptoConfig cryptoConfig) throws InvalidKeySpecException, NoSuchAlgorithmException, IOException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        this.cryptoConfig = cryptoConfig;

        // Init RSA
        PublicKey publicKey = getPublic(cryptoConfig.getRsa().getPublicKey());
        encryptRsaCipher = Cipher.getInstance(cryptoConfig.getRsa().getTransformation());
        encryptRsaCipher.init(Cipher.ENCRYPT_MODE, publicKey);

        PrivateKey privateKey = getPrivate(cryptoConfig.getRsa().getPrivateKey());
        decryptRsaCipher = Cipher.getInstance(cryptoConfig.getRsa().getTransformation());
        decryptRsaCipher.init(Cipher.DECRYPT_MODE, privateKey);

        // Init AES
        SecretKeySpec keySpec = new SecretKeySpec(Base64.getDecoder().decode(cryptoConfig.getAes().getKeyBase64()), cryptoConfig.getAes().getAlgorithm());

        encryptAesCipher = Cipher.getInstance(cryptoConfig.getAes().getTransformation());
        encryptAesCipher.init(Cipher.ENCRYPT_MODE, keySpec);

        decryptAesCipher = Cipher.getInstance(cryptoConfig.getAes().getTransformation());
        decryptAesCipher.init(Cipher.DECRYPT_MODE, keySpec);

        // Init hybrid AES-RSA-key
        byte[] key = Base64.getDecoder().decode(cryptoConfig.getHybridAesRsaKey().getKey());
        byte[] hybridAesKey = decryptRsaCipher.doFinal(key);
        SecretKeySpec hybridKeySpec = new SecretKeySpec(Base64.getDecoder().decode(hybridAesKey), cryptoConfig.getAes().getAlgorithm());

        encryptHybridCipher = Cipher.getInstance(cryptoConfig.getAes().getTransformation());
        encryptHybridCipher.init(Cipher.ENCRYPT_MODE, hybridKeySpec);

        decryptHybridCipher = Cipher.getInstance(cryptoConfig.getAes().getTransformation());
        decryptHybridCipher.init(Cipher.DECRYPT_MODE, hybridKeySpec);
    }

    private PublicKey getPublic(String key) throws InvalidKeySpecException, IOException, NoSuchAlgorithmException {
        byte[] bytes = Files.readAllBytes(Paths.get(key));
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(bytes);
        KeyFactory keyFactory = KeyFactory.getInstance(cryptoConfig.getRsa().getAlgorithm());
        return keyFactory.generatePublic(keySpec);
    }

    private PrivateKey getPrivate(String fileBase) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] bytes = Files.readAllBytes(Paths.get(fileBase));
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(bytes);
        KeyFactory keyFactory = KeyFactory.getInstance(cryptoConfig.getRsa().getAlgorithm());
        return keyFactory.generatePrivate(keySpec);
    }
}
