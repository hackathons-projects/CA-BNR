package ru.catanalysis.bnr.service;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.catanalysis.bnr.domain.model.Role;
import ru.catanalysis.bnr.domain.model.UserEntity;
import ru.catanalysis.bnr.domain.repository.UserRepository;
import ru.catanalysis.bnr.model.UserDto;

import java.util.List;
import java.util.stream.Collectors;

@Transactional
@RequiredArgsConstructor
@Service
public class UserService {

    private final ModelMapper modelMapper;
    private final UserRepository userRepository;
    private final AuthService authService;
    private final ValidationService validationService;

    public Page<UserDto> getAllUsers(Pageable pageable) {
        return userRepository.findAll(pageable).map(userEntity -> modelMapper.map(userEntity, UserDto.class));
    }

    public List<UserDto> getAllUsers() {
        return userRepository.findAll().stream().map(userEntity -> modelMapper.map(userEntity, UserDto.class)).collect(Collectors.toList());
    }

    public UserDto getUser(Long id) {
        validationService.validateAdminOrOwner(id);
        UserEntity user = userRepository.findById(id).orElseThrow();
        return modelMapper.map(user, UserDto.class);
    }

    public UserDto updatePassword(Long id, String password) {
        validationService.validateAdminOrOwner(id);
        UserEntity user = userRepository.findById(id).orElseThrow();
        user.setPassword(authService.passwordHash(password));
        UserEntity saved = userRepository.save(user);
        return modelMapper.map(saved, UserDto.class);
    }

    public UserDto changeRole(Long id, Role role) {
        UserEntity user = userRepository.findById(id).orElseThrow();

        user.setRole(role);
        UserEntity saved = userRepository.save(user);
        return modelMapper.map(saved, UserDto.class);
    }


}
