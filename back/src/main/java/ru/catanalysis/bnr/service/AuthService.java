package ru.catanalysis.bnr.service;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.catanalysis.bnr.configuration.security.JwtTokenProvider;
import ru.catanalysis.bnr.domain.model.Role;
import ru.catanalysis.bnr.domain.model.UserEntity;
import ru.catanalysis.bnr.domain.repository.UserRepository;
import ru.catanalysis.bnr.exception.CustomException;
import ru.catanalysis.bnr.model.UserDto;
import ru.catanalysis.bnr.model.request.SignInRequest;
import ru.catanalysis.bnr.model.response.SignInResponse;

@Transactional
@RequiredArgsConstructor
@Service
public class AuthService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;
    private final ModelMapper modelMapper;

    public UserDto getCurrentUser() {
        try {
            return (UserDto) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        } catch (Exception ignored) {
            throw new CustomException("Not Authorized", HttpStatus.FORBIDDEN);
        }
    }

    public boolean isAdmin() {
        return Role.ADMIN.equals(getCurrentUser().getRole());
    }

    public boolean isOwner(Long userId) {
        return userId.equals(getCurrentUser().getId());
    }

    public String passwordHash(String password) {
        return passwordEncoder.encode(password);
    }

    public SignInResponse login(SignInRequest signInRequest) {
        UserEntity user;
        switch (signInRequest.getGrantType()) {
            case PASSWORD:
                user = userRepository.findByEmail(signInRequest.getEmail()).orElseThrow(() -> new CustomException("Not authorized", HttpStatus.NOT_FOUND));
                if (passwordEncoder.matches(signInRequest.getPassword(), user.getPassword())) {
                    UserDto userDto = modelMapper.map(user, UserDto.class);
                    String accessToken = jwtTokenProvider.createAccessToken(user.getEmail(), userDto);
                    String refreshToken = jwtTokenProvider.createRefreshToken(user.getEmail());
                    return new SignInResponse(accessToken, refreshToken, userDto);
                } else {
                    throw new CustomException("Not authorized", HttpStatus.UNAUTHORIZED);
                }
            case REFRESH_TOKEN:
                if (signInRequest.getRefreshToken() != null && jwtTokenProvider.validateToken(signInRequest.getRefreshToken())) {
                    String username = jwtTokenProvider.getUsername(signInRequest.getRefreshToken());
                    user = userRepository.findByEmail(username).orElseThrow(() -> new CustomException("Not authorized", HttpStatus.NOT_FOUND));
                    UserDto userDto = modelMapper.map(user, UserDto.class);
                    String accessToken = jwtTokenProvider.createAccessToken(user.getEmail(), userDto);
                    String refreshToken = signInRequest.getRefreshToken();
                    return new SignInResponse(accessToken, refreshToken, userDto);
                } else {
                    throw new CustomException("Not authorized", HttpStatus.UNAUTHORIZED);
                }
            default:
                return null;
        }

    }
}
