package ru.catanalysis.bnr.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Slf4j
@Transactional
@RequiredArgsConstructor
@Service
public class TestService {

    private final CryptoService cryptoService;

    @PostConstruct
    private void test() throws Exception {
        testAes();
        testRsa();
        testAes_RsaKey();
    }

    private void testAes() throws Exception {
        String src = "Тестовая строка TEST";
        byte[] bbb = cryptoService.getEncryptAesCipher().doFinal(src.getBytes(StandardCharsets.UTF_8));
        System.out.println(new String(bbb));
        System.out.println(new String(Base64.getEncoder().encode(bbb)));


        byte[] rrr = cryptoService.getDecryptAesCipher().doFinal(bbb);
        System.out.println(new String(rrr));
        System.out.println();
    }

    private void testRsa() throws BadPaddingException, IllegalBlockSizeException {
        String myString = "Моя строка";
        byte[] bytes = cryptoService.getEncryptRsaCipher().doFinal(myString.getBytes(StandardCharsets.UTF_8));
        System.out.println(new String(bytes));

        byte[] res = cryptoService.getDecryptRsaCipher().doFinal(bytes);
        System.out.println(new String(res));
        System.out.println();
    }

    private void testAes_RsaKey() throws BadPaddingException, IllegalBlockSizeException {
        String s = "Э-э-эх";
        byte[] bbb = cryptoService.getEncryptHybridCipher().doFinal(s.getBytes(StandardCharsets.UTF_8));
        System.out.println(new String(bbb));


        byte[] rrr = cryptoService.getDecryptHybridCipher().doFinal(bbb);
        System.out.println(new String(rrr));
        System.out.println();
    }


}
