package ru.catanalysis.bnr.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.catanalysis.bnr.configuration.QrConfig;
import ru.catanalysis.bnr.domain.model.TicketEntity;
import ru.catanalysis.bnr.domain.repository.CheckPointRepository;
import ru.catanalysis.bnr.domain.repository.NaturalReserveRepository;
import ru.catanalysis.bnr.domain.repository.TicketRepository;
import ru.catanalysis.bnr.domain.repository.TrackRepository;
import ru.catanalysis.bnr.exception.CustomException;
import ru.catanalysis.bnr.model.QrTicketDto;
import ru.catanalysis.bnr.model.TicketDto;
import ru.catanalysis.bnr.model.request.BuyTicketRequest;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.Base64;

@Transactional
@RequiredArgsConstructor
@Service
public class TicketService {

    public static final String QR_EXTENSION = ".png";
    public static final String QR_MEDIA_TYPE = "image/png";
    private static final String QR_FORMAT = "PNG";

    private final QrConfig qrConfig;
    private final ObjectMapper objectMapper;
    private final ModelMapper modelMapper;
    private final ValidationService validationService;
    private final TicketRepository ticketRepository;
    private final TrackRepository trackRepository;
    private final NaturalReserveRepository naturalReserveRepository;
    private final CheckPointRepository checkPointRepository;
    private final CryptoService cryptoService;

    public TicketDto buyTicket(BuyTicketRequest request) {
        validationService.validateTicketDates(request);

        TicketEntity saved = ticketRepository.save(new TicketEntity(
                trackRepository.findById(request.getTrackId()).orElseThrow(),
                naturalReserveRepository.findById(request.getNaturalReserveId()).orElseThrow(),
                checkPointRepository.findAllByIdIn(request.getCheckPointIds()),
                request.getEmail(),
                request.getName(),
                request.getQuantity(),
                request.getPhone(),
                modelMapper.map(request.getStartTripDate(), LocalDate.class),
                modelMapper.map(request.getEndTripDate(), LocalDate.class)
        ));
        return modelMapper.map(saved, TicketDto.class);
    }

    // Original
    public ByteArrayInputStream generateQRBase64ByTicketIdAsStream(Long ticketId) {
        QrTicketDto ticketDto = modelMapper.map(ticketRepository.findById(ticketId).orElseThrow(), QrTicketDto.class);
        try {
            return new ByteArrayInputStream(generateQRBase64Bytes(objectMapper.writeValueAsString(ticketDto), qrConfig.getX(), qrConfig.getY()));
        } catch (WriterException | IOException e) {
            throw new CustomException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public String generateQRBase64ByTicketId(Long ticketId) {
        QrTicketDto ticketDto = modelMapper.map(ticketRepository.findById(ticketId).orElseThrow(), QrTicketDto.class);
        try {
            return generateQRBase64(objectMapper.writeValueAsString(ticketDto), qrConfig.getX(), qrConfig.getY());
        } catch (WriterException | IOException e) {
            throw new CustomException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Aes encrypted
    public String generateEncryptedQRBase64ByTicketId(Long ticketId) {
        QrTicketDto ticketDto = modelMapper.map(ticketRepository.findById(ticketId).orElseThrow(), QrTicketDto.class);

        try {
            return generateQRBase64(Base64.getEncoder().encodeToString(cryptoService.getEncryptHybridCipher().doFinal(
                    objectMapper.writeValueAsString(ticketDto).getBytes(StandardCharsets.UTF_8))),
                    qrConfig.getX(), qrConfig.getY());
        } catch (IllegalBlockSizeException | BadPaddingException | WriterException | IOException e) {
            throw new CustomException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public InputStream generateEncryptedQRBase64ByTicketIdAsStream(Long ticketId) {
        QrTicketDto ticketDto = modelMapper.map(ticketRepository.findById(ticketId).orElseThrow(), QrTicketDto.class);
        try {
            return new ByteArrayInputStream(generateQRBase64Bytes(
                    Base64.getEncoder().encodeToString(cryptoService.getEncryptHybridCipher().doFinal(
                            objectMapper.writeValueAsString(ticketDto).getBytes(StandardCharsets.UTF_8))),
                    qrConfig.getX(), qrConfig.getY()));
        } catch (WriterException | IOException | IllegalBlockSizeException | BadPaddingException e) {
            throw new CustomException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private byte[] generateQRBase64Bytes(String text, int width, int height) throws WriterException, IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
        MatrixToImageWriter.writeToStream(bitMatrix, QR_FORMAT, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }


    private String generateQRBase64(String text, int width, int height) throws WriterException, IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
        MatrixToImageWriter.writeToStream(bitMatrix, QR_FORMAT, byteArrayOutputStream);
        return Base64.getEncoder().encodeToString(byteArrayOutputStream.toByteArray());
    }
}
