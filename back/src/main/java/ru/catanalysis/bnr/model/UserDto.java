package ru.catanalysis.bnr.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.catanalysis.bnr.domain.model.Gender;
import ru.catanalysis.bnr.domain.model.Role;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserDto {

    private Long id;

    private String email;

    private String password;

    private Role role;

    private String name;

    private String birthDay;

    private Gender gender;

    private String passportNumber;

    private String city;
}
