package ru.catanalysis.bnr.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.catanalysis.bnr.domain.model.CheckPointEntity;
import ru.catanalysis.bnr.domain.model.NaturalReserveEntity;
import ru.catanalysis.bnr.domain.model.TrackEntity;

import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TicketDto {

    private Long id;

    private TrackEntity track;

    private NaturalReserveEntity naturalReserve;

    private Set<CheckPointEntity> checkPoints = new HashSet<>();

    private String email;

    private String name;

    private Integer quantity;

    private String phone;

    private String startTripDate;

    private String endTripDate;

}
