package ru.catanalysis.bnr.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class QrTicketDto {

    private Long id;

    private String trackName;

    private String naturalReserveName;

    private Set<Long> checkPointIds = new HashSet<>();

    private String email;

    private String name;

    private Integer quantity;

    private String phone;

    private String startTripDate;

    private String endTripDate;

    private String signature;
}
