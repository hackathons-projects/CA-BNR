package ru.catanalysis.bnr.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.catanalysis.bnr.configuration.security.GrantType;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SignInRequest {

    private String email;

    private String password;

    private String token;

    private String refreshToken;

    private GrantType grantType;
}
