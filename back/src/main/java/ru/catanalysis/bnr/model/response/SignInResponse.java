package ru.catanalysis.bnr.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.catanalysis.bnr.model.UserDto;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class SignInResponse {

    private String accessToken;

    private String refreshToken;

    private UserDto user;
}
