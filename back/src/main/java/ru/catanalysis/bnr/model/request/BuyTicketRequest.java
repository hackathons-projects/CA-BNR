package ru.catanalysis.bnr.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class BuyTicketRequest {

    @NotNull
    private Long trackId;

    @NotNull
    private Long naturalReserveId;

    @NotNull
    private List<Long> checkPointIds;

    private String email;

    private String name;

    private Integer quantity;

    private String phone;

    private String startTripDate;

    private String endTripDate;
}
