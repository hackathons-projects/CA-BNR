package ru.catanalysis.bnr.configuration.security;

public enum GrantType {
    PASSWORD, REFRESH_TOKEN
}
