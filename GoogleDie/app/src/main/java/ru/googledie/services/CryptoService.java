package ru.googledie.services;

import androidx.appcompat.app.AppCompatActivity;
import org.apache.commons.io.IOUtils;
import ru.googledie.MainActivity;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class CryptoService {

    private final AppCompatActivity mainActivity;

    private static final String AES_ALGORITHM = "AES";
    private static final String AES_KEY = "vlM8BvMLT6GFV2NJK0lei4rLoXXQsodoNKzP1SjK2t8=";
    private static final String AES_TRANSFORMATION= "AES/ECB/PKCS5Padding";

    private static final String RSA_ALGORITHM = "RSA";
    private static final String PUB_RSA = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAupR7+UF/LwNrdKE+mCom2eFZJgatk5Vbj/4alBttVjtKymzIlDRnVavWoD/nu6Ynl5to2esJocqEQ2g2vA/HUCJdhRbILqW54L/Cc7C6qSWOnVgQ2t4b/7AD6yMF3TT3m8CZAMGkBLshWfNsmdZuH1WYrYyfnvFdmWpITaP59izxAxOEI96bJlSwL9Z04r18BUY9jr2WYhgQ4gQNUzKTvgJtt3kzRn1PZW4Sq1sYIqURDh7vrdqakWBAFlHtgTom0N7U9+mNq57NWR2qLHunUcGQ7ACQ5v4cNU3rP0FFmfEYAPJfbWxEtr2Hk3jtFqB5LyzZhNhTYdyyVUa589RezQIDAQAB";
//    private static final String RSA_KEY = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC6lHv5QX8vA2t0oT6YKibZ4VkmBq2TlVuP/hqUG21WO0rKbMiUNGdVq9agP+e7pieXm2jZ6wmhyoRDaDa8D8dQIl2FFsgupbngv8JzsLqpJY6dWBDa3hv/sAPrIwXdNPebwJkAwaQEuyFZ82yZ1m4fVZitjJ+e8V2ZakhNo/n2LPEDE4Qj3psmVLAv1nTivXwFRj2OvZZiGBDiBA1TMpO+Am23eTNGfU9lbhKrWxgipREOHu+t2pqRYEAWUe2BOibQ3tT36Y2rns1ZHaose6dRwZDsAJDm/hw1Tes/QUWZ8RgA8l9tbES2vYeTeO0WoHkvLNmE2FNh3LJVRrnz1F7NAgMBAAECggEBAJAld0Iy39eqhLIugPV+W1WpS/6c2i1TDtJINrCGIAqm9Dk/ohceBVei0sYrmRTYYW43muIBPAfLNjP9p4vThODcK/ROjYm8b8a7X7eRqiRT58KX7y2ou2jmy6A2BqnH6iRiv9JdyCH/kNy1vAl+KMX8k2BBbHCXeQC1o/aX4N3WuCFS0i5xjAFcAvouicPii6JPTK7glY766MgjA8zgFxouRLY1O0ZcDtAjfUnDcTkYvzwBl5J8J0FOl/ks9kvfWWpIC/WGnj9m1p7rRHbkNW+mSlMzmkPg196jSrSil1aKi4OakSHsCVz2yLm3yR2fGzL4nzYwvtEzKoqcC0uK3+ECgYEA6zMeeXovJkElvVEas7obnwIfsiusptBlpjk8HAOfso3qLZoaQI2SFeaRTIs6T1UWcIBHExISjjB3USlDpV7EidXZM4Oc/pBisnoeF68mTru5QUD2G1wzJD5Cm0Bq68xp0Bno7wiYqVMY/pdNKnuUReh3bk2xARa3C2haVDZIFwMCgYEAyxSfzGflmucA5WjmCz+gNRXIWDAnr580VtLzKZNYnQUxIcvGprN2LiYQ2C31NLzXg5v0pKW2hnc1QOqvT5KtFQYtrw6tx1cSvGTqHgbbDDMJHDW1pqZIQoO/1A+YxIqThsToaK+XFD62WB7VOdySzzxqVb5AqFFycf1FmPNaoe8CgYEAkP+j0vo7pRbWwF8G2jRrIN07UylPuEuTzyVL5G1rf7Wz4Ec31gIxRKUvgIP4/72SJus+ZghnPhetB3EsDgBdpCgdTn/eQqYa4rMcaDeHo1RiAlOdJgLyWG61u2a8RAByX2QJ0Yt4KjNbnmECpqUnJO4K6wqki+pIEJVV0sLgMKcCgYBBPlQEeJCkjniUO3pEvOE8jM37vyTQ4GYRSt6CSetbLvLU98HPhrJ+Kw6YVVOE6PKYPr9/MhoM4zmBrKQ7/VSL/5sntaCQ/WFwkz8//FXdenv/yyWb76ohbHBxsb0Tz62Ly7EbCOaRw4ATmXxJhZrI55EGEQR2zwvkoUj91ZkVkwKBgDfWcPdbVvjQRBUDUffNC1PRzLgvj0pNvK31PJM8owG2z7E0N7dc5JE60HikWzANjTxYKOHKAg1Iu6CKYLDeZ9IYUA0ej/TcqABPHWZKVH2Tw6jSo5BN41JFe1WfHyv/csWTjOLlzPbN2Pj3rWocwwip23wV7TDHuwDiI3MUSNNP";
    private static final String RSA_TRANSFORMATION= "RSA/ECB/PKCS1Padding";

//    private final Cipher encryptRsaCipher;
//    private final Cipher decryptRsaCipher;
    private final Cipher encryptAesCipher;
    private final Cipher decryptAesCipher;
//    private final Cipher encryptHybridCipher;
//    private final Cipher decryptHybridCipher;

    public CryptoService(AppCompatActivity mainActivity) throws InvalidKeySpecException, NoSuchAlgorithmException, IOException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        this.mainActivity = mainActivity;

//        // Init RSA
//        PublicKey publicKey = getPublic(RSA_KEY);
//        encryptRsaCipher = Cipher.getInstance(RSA_TRANSFORMATION);
//        encryptRsaCipher.init(Cipher.ENCRYPT_MODE, publicKey);

//        PrivateKey privateKey = getPrivate(RSA_KEY);
//        decryptRsaCipher = Cipher.getInstance(RSA_TRANSFORMATION);
//        decryptRsaCipher.init(Cipher.DECRYPT_MODE, privateKey);

        // Init AES
        SecretKeySpec keySpec = new SecretKeySpec(Base64.getDecoder().decode(AES_KEY), AES_ALGORITHM);

        encryptAesCipher = Cipher.getInstance(AES_TRANSFORMATION);
        encryptAesCipher.init(Cipher.ENCRYPT_MODE, keySpec);

        decryptAesCipher = Cipher.getInstance(AES_TRANSFORMATION);
        decryptAesCipher.init(Cipher.DECRYPT_MODE, keySpec);

//        // Init hybrid AES-RSA-key
//        byte[] key = Base64.getDecoder().decode(cryptoConfig.getHybridAesRsaKey().getKey());
//        byte[] hybridAesKey = decryptRsaCipher.doFinal(key);
//        SecretKeySpec hybridKeySpec = new SecretKeySpec(Base64.getDecoder().decode(hybridAesKey), cryptoConfig.getAes().getAlgorithm());
//
//        encryptHybridCipher = Cipher.getInstance(cryptoConfig.getAes().getTransformation());
//        encryptHybridCipher.init(Cipher.ENCRYPT_MODE, hybridKeySpec);
//
//        decryptHybridCipher = Cipher.getInstance(cryptoConfig.getAes().getTransformation());
//        decryptHybridCipher.init(Cipher.DECRYPT_MODE, hybridKeySpec);
    }

//    private PublicKey getPublic(String key) throws InvalidKeySpecException, IOException, NoSuchAlgorithmException {
//        byte[] bytes = Files.readAllBytes(Paths.get(key));
//        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(bytes);
//        KeyFactory keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);
//        return keyFactory.generatePublic(keySpec);
//    }

//    private PrivateKey getPrivate(String fileBase) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
//        byte[] bytes = Base64.getDecoder().decode(RSA_KEY);
//        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(bytes);
//        KeyFactory keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);
//        return keyFactory.generatePrivate(keySpec);
//    }


    private PublicKey getPublic(String key) throws InvalidKeySpecException, IOException, NoSuchAlgorithmException {
        byte[] bytes = Base64.getDecoder().decode(key);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(bytes);
        KeyFactory keyFactory = KeyFactory.getInstance(RSA_ALGORITHM);
        return keyFactory.generatePublic(keySpec);
    }

    public Signature getSignatureForVerify() {
        try {
            Signature signature = Signature.getInstance("SHA256withRSA");
            signature.initVerify(getPublic(PUB_RSA));
            return signature;
        } catch (NoSuchAlgorithmException | InvalidKeyException | InvalidKeySpecException | IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }




    public Cipher getEncryptAesCipher() {
        return encryptAesCipher;
    }

    public Cipher getDecryptAesCipher() {
        return decryptAesCipher;
    }

//    public Cipher getEncryptRsaCipher() {
//        return encryptRsaCipher;
//    }

//    public Cipher getDecryptRsaCipher() {
//        return decryptRsaCipher;
//    }
}
