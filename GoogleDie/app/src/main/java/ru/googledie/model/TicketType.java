package ru.googledie.model;

public enum TicketType {
    TEMPORARY, REGULAR, DISPOSABLE
}
