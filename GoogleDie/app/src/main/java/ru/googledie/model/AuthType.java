package ru.googledie.model;

public enum AuthType {
    QR, BLE, NFC, FACE, VOICE
}
