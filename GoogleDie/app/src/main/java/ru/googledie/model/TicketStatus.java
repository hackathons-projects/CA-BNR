package ru.googledie.model;

public enum TicketStatus {
    REQUEST, APPROVED, REVOKED, EXPIRED
}
